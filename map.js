var mymap = L.map('mapid').setView([45.564601, 5.917781], 15);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYXVyb3JlaHVhdmFuc2ltcGxvbiIsImEiOiJjanVhemg2YncwNmo3NDFvOHZlNTNvOWw3In0.1KyPo4hJpGe6UmObpIrKNA'
}).addTo(mymap);


/* var polygon = L.polygon([ // draw a polygon between all points
    [45.5636, 5.924191],
    [45.56511, 5.922409],
    [45.56413, 5.925546]
]).addTo(mymap); */

//L.marker([45.619353, 5.887191]).addTo(map);

// try popup bar
/* var popup1 = L.popup() // open a card with the content (but show just the last)
    .setLatLng([45.5636, 5.924191])
    .setContent("O'POGUES")
    .openOn(mymap); */

/* //view coordonates on click
function onMapClick(e) {
    alert("You clicked the map at " + e.latlng);
}

mymap.on('click', onMapClick); */

// ex change the icon
/* let greenIcon = L.icon({
    iconUrl: 'images/marker_leaf-green.png',
    iconSize: [30, 50], // size of the icon
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor: [3, 13] // point from which the popup should open relative to the iconAnchor
});
L.marker([45.619353, 5.887191], {
    icon: greenIcon
}).addTo(mymap);
 */

//geolocalization
let meIcon = L.icon({
    iconUrl: 'images/computers.png',
    iconSize: [30, 50], // size of the icon
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor: [3, 13] // point from which the popup should open relative to the iconAnchor
});

function initElement() {
    let p = document.getElementById("geolocalisation");
    p.onclick = currentLocation;
}

function currentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((function (position) {
            let marker = L.marker([position.coords.latitude, position.coords.longitude], {
                icon: meIcon
                //icon: myicon
            }).addTo(mymap);
            marker.bindPopup("Ma position :<br> Latitude : " + position.coords.latitude + ',<br>Longitude ' + position.coords.longitude).openPopup();
            //center: new google.maps.latLng(lat, lng)
        }));
    } else {
        alert("La géolocalisation n'est pas supportée par ce navigateur.");
    }
}